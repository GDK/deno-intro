export const ISSConfig = {
	url: 'http://api.open-notify.org/iss-now.json',
	delay: 1000
};

export interface IAPIConfig {
	url: string
};

export const APIConfig: IAPIConfig = {
	url: ISSConfig.url
};