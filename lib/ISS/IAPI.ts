export interface IISSPosition {
	latitude: number;
	longitude: number;
}

export interface IISSLocation {
	message: string;
	timestamp: number;
	position: IISSPosition;
}

export interface IISSApi {
	get(): Promise<IISSLocation>
}