export interface IServer {
	initialize(): void;
	run(): void
}