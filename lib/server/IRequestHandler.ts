export interface IRequestHandler<R, A, B> {
	handle(request: R): Promise<A> | A;
	withBody(body: B): IRequestHandler<R, A, B>;
}

export const ContentTypeApplicationJSON = {
	name: 'content-type',
	value: 'application/json'
};