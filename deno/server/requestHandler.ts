import { ServerRequest, Response } from "https://deno.land/std/http/server.ts";

import {
  IRequestHandler,
  ContentTypeApplicationJSON,
} from "../../lib/server/IRequestHandler.ts";

export class RequestHandler
  implements IRequestHandler<ServerRequest, Response, Uint8Array> {
  private textDecoder: TextDecoder;
  private body: Uint8Array | undefined;

  constructor() {
    this.body = undefined;
    this.textDecoder = new TextDecoder("utf-8");
  }

  withBody(
    body: Uint8Array,
  ): IRequestHandler<ServerRequest, Response, Uint8Array> {
    this.body = body;
    return this;
  }

  handle(request: ServerRequest): Promise<Response> | Response {
    const res: Response = {
      status: 200,
      headers: new Headers([
        [ContentTypeApplicationJSON.name, ContentTypeApplicationJSON.value],
      ]),
      body: JSON.stringify({
        url: request.url,
        method: request.method,
        body: this.textDecoder.decode(this.body),
      }),
    };

    return res;
  }
}
