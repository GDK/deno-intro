import { DenoServerConfig } from "../../lib/server/config.ts";

import { DenoServer } from "./server.ts";
import { RequestHandler } from "./requestHandler.ts";

const requestHandler = new RequestHandler();
const server = new DenoServer(DenoServerConfig, requestHandler);

server.initialize();
server.run();
