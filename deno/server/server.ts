import {
  serve,
  Server,
  ServerRequest,
  Response,
} from "https://deno.land/std/http/server.ts";

import { IServer } from "../../lib/server/IServer.ts";
import { IServerConfig } from "../../lib/server/config.ts";
import { IRequestHandler } from "../../lib/server/IRequestHandler.ts";

export class DenoServer implements IServer {
  private server: Server | undefined;
  private config: IServerConfig;
  private requestHandler: IRequestHandler<ServerRequest, Response, Uint8Array>;

  constructor(
    config: IServerConfig,
    requestHandler: IRequestHandler<ServerRequest, Response, Uint8Array>,
  ) {
    this.server = undefined;
    this.config = config;
    this.requestHandler = requestHandler;
  }

  initialize(): void {
  }

  run(): void {
    this.runAsync();
  }

  async runAsync(): Promise<void> {
    this.server = serve({ port: this.config.port });
    console.log(`Server lisiting at port ${this.config.port}`);

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of
    for await (const req of this.server) {
      console.log(`Incoming request [${new Date().toISOString()}][${req.method}][${req.url}]`,
      );

      const body = await Deno.readAll(req.body);
      const response = await this.requestHandler
        .withBody(body)
        .handle(req);
      req.respond(response);
    }
  }
}
