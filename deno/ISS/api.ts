import { IAPIConfig } from "../../lib/ISS/config.ts";
import { IISSApi, IISSLocation } from "../../lib/ISS/IAPI.ts";

export class DenoAPI implements IISSApi {
  private config: IAPIConfig;
  private url: URL;
  private requestInfo: RequestInfo;

  constructor(config: IAPIConfig) {
    this.config = config;
    this.url = new URL(this.config.url);
    this.requestInfo = this.url.href;
  }

  async get(): Promise<IISSLocation> {
    let responseBody: IISSLocation = {
      message: "error",
      timestamp: 0,
      position: {
        latitude: 0,
        longitude: 0,
      },
    };

    const response: Response = await fetch(this.requestInfo);

    if (response.status < 300) {
      responseBody = await response.json();
    }

    return responseBody;
  }
}
