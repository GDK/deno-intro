import { IISSApi } from "../../lib/ISS/IAPI.ts";
import { ISSConfig, APIConfig } from "../../lib/ISS/config.ts";

import { DenoAPI } from "./api.ts";

const api: IISSApi = new DenoAPI(APIConfig);

const pollISSLocation = (api: IISSApi, delay: number) => {
  const timeout = setTimeout((args: any[]) => {
      api.get()
        .then((response) => {
          console.log(JSON.parse(JSON.stringify(response)));

          clearTimeout(timeout);
          pollISSLocation(args[0], args[1]);
        })
        .catch((error) => {
          clearTimeout(timeout);
          console.error(error);
        });
    },
    delay,
    [api, delay],
  );
};

pollISSLocation(api, ISSConfig.delay);
