import * as http from 'http';

import { IRequestHandler, ContentTypeApplicationJSON } from "../../../lib/server/IRequestHandler";

export interface IRequestResponse {
	request: http.IncomingMessage;
	response: http.ServerResponse;
}

export class RequestHandler 
	implements IRequestHandler<IRequestResponse, http.ServerResponse, any> {
	private body: any;

	withBody(body: any): IRequestHandler<IRequestResponse, http.ServerResponse, any> {
		this.body = body;
		return this;
	}

	handle(requestResponse: IRequestResponse): Promise<http.ServerResponse> | http.ServerResponse {
		this.setResponse(requestResponse.request, requestResponse.response);
		return requestResponse.response;
	}

	private setResponse(request: http.IncomingMessage, response: http.ServerResponse): void {
		const body = {
			url: request.url,
			method: request.method,
			body: this.body
		};

		response.statusCode = 200;
		response.setHeader(ContentTypeApplicationJSON.name, ContentTypeApplicationJSON.value);
		response.end(body);
	}
}