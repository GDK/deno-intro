import { NodeServerConfig } from "../../../lib/server/config";

import { NodeServer } from "./server";
import { RequestHandler } from "./requestHandler";

const requestHandler = new RequestHandler();
const server = new NodeServer(NodeServerConfig, requestHandler);

server.initialize();
server.run();