import * as http from 'http';

import { IServer } from "../../../lib/server/IServer";
import { IServerConfig } from '../../../lib/server/config';
import { IRequestHandler } from "../../../lib/server/IRequestHandler";

import { IRequestResponse } from './requestHandler'

export class NodeServer implements IServer {
	private server: http.Server | undefined;
	private config: IServerConfig;
	private requestHandler: IRequestHandler<IRequestResponse, http.ServerResponse, any>;

	constructor(config: IServerConfig, requestHandler: IRequestHandler<IRequestResponse, http.ServerResponse, any>) {
		this.server = undefined;
		this.config = config;
		this.requestHandler = requestHandler;
	}

	initialize(): void {
		this.server = http.createServer((req, res) => {
			console.log(`Incoming request [${new Date().toISOString()}][${req.method}][${req.url}]`);

			let body: any = '';
			req.on('data', (chunk) => {
				body += chunk;
			});

			req.on('end', () => {
				this.requestHandler
					.withBody(body)
					.handle({
						request: req,
						response: res
					});
			});

		});
	}

	run(): void {
		this.server.listen(this.config.port, () => {
			console.log(`Server lisiting at port ${this.config.port}`);
		});
	}
}