import { IISSApi } from "../../../lib/ISS/IAPI";
import { ISSConfig, APIConfig } from "../../../lib/ISS/config";

import { NodeAPI } from "./api";

const api: IISSApi = new NodeAPI(APIConfig);

const pollISSLocation = (api: IISSApi, delay: number) => {
	const timeout = setTimeout((args) => {
		api.get()
			.then(response => {
				console.log(JSON.parse(JSON.stringify(response)));

				pollISSLocation(args[0], args[1]);
			})
			.catch(error => {
				console.error(error);
			})
			.finally(() => {
				clearTimeout(timeout);
			})
	}, delay, [api, delay]);
};

pollISSLocation(api, ISSConfig.delay);