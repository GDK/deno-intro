import * as http from 'http';

import { IAPIConfig } from "../../../lib/ISS/config";
import { IISSApi, IISSLocation } from "../../../lib/ISS/IAPI";

export class NodeAPI implements IISSApi {
	private config: IAPIConfig;
	private url: URL;

	constructor(config: IAPIConfig) {
		this.config = config;

		this.url = new URL(this.config.url);
	}

	async get(): Promise<IISSLocation> {
		const requestResponse = await this.sendRequest();
		const { body } = requestResponse;

		let responseBody: IISSLocation = {
			message: 'error',
			timestamp: 0,
			position: {
				latitude: 0,
				longitude: 0
			}
		};

		if (typeof body === 'string') {
			responseBody = JSON.parse(body);
		} else {
			responseBody.message += ': Body was not a string';
		}

		return responseBody;
	}

	private sendRequest(): Promise<any> {
		return new Promise((resolve, reject) => {
			const request = http.request(this.url, (response: http.IncomingMessage) => {
				if (response.statusCode >= 300) {
					reject(response);
					return;
				}

				let body = '';
				response.setEncoding('utf-8');
				response.on('data', (chunk) => {
					body += chunk;
				});

				response.on('error', (err) => reject(err));
				response.on('end', () => resolve({
					response,
					body
				}));
			});

			request.on('error', (err) => reject(err));
			request.end();
		});
	}
};